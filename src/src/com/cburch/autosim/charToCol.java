/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cburch.autosim;

import java.awt.Color;

/**
 *
 * @author john
 */
public class charToCol {
    
    public static Color charToCol(char c){
        switch(c) {
            case 'r':return Color.red;
            case 'b':return Color.blue;
            case 'o':return Color.orange;
            case 'g':return Color.green;
            case 'p':return Color.PINK;
            case Alphabet.BLANK:return Color.BLACK;
            case Alphabet.ELSE: return Color.LIGHT_GRAY;
            default: return Color.GRAY;
            
        }
    }
}
